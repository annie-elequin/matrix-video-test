import React, {useEffect, useState} from 'react';
import {SafeAreaView, View, Text, Pressable} from 'react-native';

import {matrix} from '@rn-matrix/core';
import {RoomList, MessageList} from '@rn-matrix/ui';

const homeserver = 'https://matrix.ditto.chat';
const mxid = '@test:ditto.chat';
const accessToken =
  'MDAxOGxvY2F0aW9uIGRpdHRvLmNoYXQKMDAxM2lkZW50aWZpZXIga2V5CjAwMTBjaWQgZ2VuID0gMQowMDIzY2lkIHVzZXJfaWQgPSBAdGVzdDpkaXR0by5jaGF0CjAwMTZjaWQgdHlwZSA9IGFjY2VzcwowMDIxY2lkIG5vbmNlID0gLVZuX3RZVjJpRk80V2QsKwowMDJmc2lnbmF0dXJlIGYlwrKiStuijF4uaQ9KJStxRDueNHpAT3b74ZaZI-n_Cg';
const deviceId = 'EBIPBHNMDO';

const App = () => {
  const [room, setRoom] = useState(null);

  const handleRoomPress = (r) => {
    setRoom(r);
  };

  useEffect(() => {
    matrix.createClient(homeserver, accessToken, mxid, deviceId);
    matrix.start(true);
  }, []);

  if (room) {
    return (
      <SafeAreaView style={{flex: 1}}>
        <BackButton onPress={() => setRoom(null)} />
        <MessageList room={room} keyboardOffset={90} enableComposer />
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={{flex: 1}}>
        <RoomList onRowPress={handleRoomPress} />
      </SafeAreaView>
    );
  }
};

export default App;

function BackButton({onPress}) {
  return (
    <Pressable
      onPress={onPress}
      style={({pressed}) => ({
        backgroundColor: 'dodgerblue',
        width: '100%',
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        opacity: pressed ? 0.5 : 1,
      })}>
      <Text style={{color: 'white', fontWeight: 'bold'}}>BACK</Text>
    </Pressable>
  );
}
