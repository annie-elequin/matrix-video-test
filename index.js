/**
 * @format
 */
import 'node-libs-react-native/globals';

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

import {polyfillGlobal} from 'react-native/Libraries/Utilities/PolyfillFunctions';
polyfillGlobal('URL', () => require('whatwg-url').URL);

AppRegistry.registerComponent(appName, () => App);
